const defaultProducts = [
	{
		name: "Xiaomi Redmi Note 10 Versão Global 6GB+128GB Lake Green",
		price: 1562.0,
		img: "https://m.media-amazon.com/images/I/41NXqsktNvS._AC_UL320_.jpg",
	},
	{
		name: "Xiaomi Poco M3 4GB+128GB Versão Global (Yellow)",
		price: 1149.0,
		img: "https://m.media-amazon.com/images/I/61RW+Sov9XL._AC_UL320_.jpg",
	},
	{
		name: 'Celular Xiaomi Mi 10T 128GB 6GB Ram 5G Dual Sim Tela 6,67" Câmeras 64MP+13MP+5MP e 20MP - Lunar Silver',
		price: 2684.0,
		img: "https://m.media-amazon.com/images/I/61ZVJqFdLlL._AC_UL320_.jpg",
	},
	{
		name: "Smartphone Xiaomi Redmi 9A 2/32GB - Azul",
		price: 708.07,
		img: "https://m.media-amazon.com/images/I/51bseWUDNOL._AC_UL320_.jpg",
	},
	{
		name: "CELULAR XIAOMI REDMI NOTE 8 DUAL 128GB NEPTUNE BLUE",
		price: 1259.99,
		img: "https://m.media-amazon.com/images/I/51KdQevjaXL._AC_UL320_.jpg",
	},
]

export default defaultProducts
