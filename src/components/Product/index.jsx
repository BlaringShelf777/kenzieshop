import { useDispatch } from "react-redux"
import addToCartThunk from "../../store/modules/cart/thunks"
import { Container } from "./styles"

const Product = ({ product }) => {
	const { img, name, price } = product
	const dispatch = useDispatch()

	const parsePrice = (price) =>
		Intl.NumberFormat("pt-BR", {
			style: "currency",
			currency: "BRL",
		}).format(price)

	const parseName = (name) => {
		const _name = name.toLowerCase()

		if (name.length > 50) {
			return _name.slice(0, 51).trim() + "..."
		}

		return _name
	}

	const handleClick = () => dispatch(addToCartThunk(product))

	return (
		<Container>
			<img src={img} alt={name} />
			<hr />
			<h3>{parseName(name)}</h3>
			<p>{parsePrice(price)}</p>
			<button onClick={handleClick}>Add to cart</button>
		</Container>
	)
}

export default Product
