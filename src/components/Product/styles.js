import styled from "styled-components"

export const Container = styled.div`
	max-width: 250px;
	height: 350px;
	padding: 1rem;
	font-family: sans-serif;
	display: flex;
	flex-direction: column;
	gap: 0.5rem;
	align-items: center;
	justify-content: space-between;
	background-color: white;

	img {
		height: 180px;
	}

	hr {
		width: 100%;
	}

	h3 {
		font-weight: normal;
		font-size: 1rem;
		text-transform: capitalize;
	}

	p {
		align-self: flex-start;
	}

	button {
		font-family: inherit;
		background-color: #8684fc;
		color: white;
		width: 100%;
		border: 0;
		padding: 0.5rem;
		border-radius: 10px;
	}
`
