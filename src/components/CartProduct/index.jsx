import { Container } from "./styles"

const CartProduct = ({ product }) => {
	const { img, price, name } = product

	const parsePrice = (price) =>
		Intl.NumberFormat("pt-BR", {
			style: "currency",
			currency: "BRL",
		}).format(price)

	const parseName = (name) => {
		const _name = name.toLowerCase()

		if (name.length > 50) {
			return _name.slice(0, 51).trim() + "..."
		}

		return _name
	}

	return (
		<Container>
			<img src={img} alt={name} />
			<h3>{parseName(name)}</h3>
			<p>{parsePrice(price)}</p>
		</Container>
	)
}

export default CartProduct
