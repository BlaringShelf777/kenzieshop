import styled from "styled-components"

export const Container = styled.div`
	display: flex;
	justify-content: space-between;
	align-items: center;
	padding: 2rem 1rem;
	border-bottom: 2px solid var(--gray);
	gap: 1rem;
	background-color: white;

	img {
		height: 100%;
		max-height: 150px;
	}

	h3 {
		font-weight: normal;
		font-size: 0.8rem;
	}

	p {
		font-weight: bold;
	}

	@media only screen and (max-width: 500px) {
		flex-direction: column;
	}
`
