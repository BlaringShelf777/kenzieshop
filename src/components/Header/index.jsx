import { useSelector } from "react-redux"
import { useHistory } from "react-router-dom"
import { Container } from "./styles"

const Header = () => {
	const { cart } = useSelector((store) => store)
	const history = useHistory()

	return (
		<>
			<Container>
				<div>
					<h1>KenzieShop</h1>
					<div onClick={() => history.push("/cart")}>
						<i class="fas fa-shopping-cart">
							{cart.length !== 0 && <span>{cart.length}</span>}
						</i>
						Carrinho
					</div>
				</div>
				<hr />
			</Container>
		</>
	)
}

export default Header
