import styled from "styled-components"

export const Container = styled.div`
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	padding: 1rem;
	background-color: var(--gray);

	& + div {
		margin-top: 4.8rem;
	}

	div {
		color: #2a2a28;
		display: flex;
		justify-content: space-between;
		align-items: baseline;

		div {
			cursor: pointer;

			i {
				position: relative;
				margin-right: 0.5rem;

				span {
					color: white;
					background-color: #8684fc;
					font-size: 0.7rem;
					font-weight: bold;
					display: grid;
					place-items: center;
					position: absolute;
					width: 18px;
					height: 18px;
					top: -10px;
					right: -10px;
					border-radius: 50%;
				}
			}
		}
	}
`
