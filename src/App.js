import styled from "styled-components"
import Header from "./components/Header"
import Routes from "./Routes"

const Container = styled.div`
	background-color: var(--gray);
	min-height: 100vh;
	width: 100%;
`

function App() {
	return (
		<Container>
			<Header />
			<Routes />
		</Container>
	)
}

export default App
