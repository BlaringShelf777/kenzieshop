import styled from "styled-components"

export const Container = styled.div`
	display: flex;
	justify-content: space-around;
	padding: 1rem 5rem;

	div.cart__list {
		max-width: 800px;
		box-shadow: 0 2px 5px rgba(0, 0, 0, 0.35);

		div:first-child {
			display: flex;
			justify-content: space-between;
			background-color: white;
			padding: 1rem;
			font-weight: bold;
			border-bottom: 2px solid var(--gray);
		}
	}

	div.cart__info {
		background-color: white;
		height: fit-content;
		width: 300px;
		padding: 1rem;
		box-shadow: 0 2px 5px rgba(0, 0, 0, 0.35);

		h2 {
			font-size: 1.2rem;
		}

		div {
			margin-top: 1rem;
			display: flex;
			justify-content: space-between;
		}
	}

	@media only screen and (max-width: 1080px) {
		flex-direction: column-reverse;
		padding: 1rem 2rem;
		gap: 1rem;

		div.cart__info {
			width: 100%;
		}

		div.cart__list {
			max-width: initial;
		}
	}
`
