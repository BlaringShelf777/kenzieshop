import { useSelector } from "react-redux"
import CartProduct from "../../components/CartProduct"
import { Container } from "./styles"

const Cart = () => {
	const { cart } = useSelector((store) => store)

	const parsePrice = (price) =>
		Intl.NumberFormat("pt-BR", {
			style: "currency",
			currency: "BRL",
		}).format(price)

	return (
		<Container>
			<div className="cart__list">
				<div>
					<p>Product</p>
					<p>Price</p>
				</div>
				{cart.map((product) => (
					<CartProduct product={product} />
				))}
			</div>
			<div className="cart__info">
				<h2>Order Summary</h2>
				<div>
					<p>{cart.length} Products</p>
					<p>{parsePrice(cart.reduce((acc, cur) => acc + cur.price, 0))}</p>
				</div>
			</div>
		</Container>
	)
}

export default Cart
