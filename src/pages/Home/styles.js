import styled from "styled-components"

export const Container = styled.div`
	max-width: 80%;
	margin: 0 auto;
	display: flex;
	flex-wrap: wrap;
	gap: 1rem;
	justify-content: center;
	padding: 1rem 0;
`
