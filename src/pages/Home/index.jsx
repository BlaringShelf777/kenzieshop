import defaultProducts from "../../services/products"
import { Container } from "./styles"
import Product from "../../components/Product"

const Home = () => {
	return (
		<Container>
			{defaultProducts.map((product) => (
				<Product product={product} />
			))}
		</Container>
	)
}

export default Home
