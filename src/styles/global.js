import { createGlobalStyle } from "styled-components"

export default createGlobalStyle`
    :root {
        --gray: #f5f5f2;
    }

    * {
        padding: 0;
        margin: 0;
        box-sizing: border-box;
        font-family: "Nunito", sans-serif;
    }

    button {
        cursor: pointer;
    }
`
