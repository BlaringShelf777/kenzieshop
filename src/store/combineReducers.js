import { combineReducers } from "redux"
import { cartReducer } from "./modules/cart/reducers"

export const reducers = combineReducers({
	cart: cartReducer,
})
