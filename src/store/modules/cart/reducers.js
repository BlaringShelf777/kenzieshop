import { addToCartAction } from "./actionTypes"

const initialState = JSON.parse(localStorage.getItem("@kenzieshop:cart")) || []

export const cartReducer = (state = initialState, action) => {
	switch (action.type) {
		case addToCartAction:
			return [...state, action.product]
		default:
			return state
	}
}
