import { addToCartAction } from "./actionTypes"

export const addToCart = (product) => ({
	type: addToCartAction,
	product,
})
