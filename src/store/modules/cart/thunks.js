import { addToCart } from "./actions"

const addToCartThunk = (product) => (dispatch, getState) => {
	const { cart } = getState()
	const updateStorage = [...cart, product]

	localStorage.setItem("@kenzieshop:cart", JSON.stringify(updateStorage))

	dispatch(addToCart(product))
}

export default addToCartThunk
